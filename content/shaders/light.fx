﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

matrix projection;
float2 position;
float2 resolution;
float4 color;

struct VertexShaderInput
{
	float4 pos : POSITION0;
	float4 color : COLOR0;
};

struct VertexShaderOutput
{
	float4 pos : SV_POSITION0;
	float4 color : COLOR0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.pos = mul(input.pos, projection);
	output.color = input.color;

	return output;
}

float4 MainPS(VertexShaderOutput input) : COLOR0
{
	//float x = input.pos.x;
	float2 pos = input.pos.xy;
	pos.y = resolution.y - pos.y;
	float dist = 3.0 / log2(distance(pos, position));
	return color * float4(dist, dist, dist, 1.0);
}

technique SpriteDrawing
{
	pass P0
	{
		VertexShader = compile vs_3_0 MainVS();
		PixelShader = compile ps_3_0 MainPS();
	}
};