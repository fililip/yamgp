using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace YAMGP
{
	public class Drone : PhysicsBody
    {
		public Drone()
		{
			handleInput = false;
			gravity = 0;
			airDeceleration = 60f;
		}

		protected override void HandleInput()
		{
			Vector2 axes = GamePad.GetState(0).ThumbSticks.Left;

			if (CheckKey(Keys.W)) input.Y += -2;
            if (CheckKey(Keys.S)) input.Y += 2;

			if (CheckKey(Keys.A)) input.X += -1;
            if (CheckKey(Keys.D)) input.X += 1;

			if (Math.Abs(axes.X) > float.Epsilon || Math.Abs(axes.Y) > float.Epsilon)
				axes.Normalize();
			
			input.X += axes.X;
			gravity = input.Y * 12f;

            input *= 32f;
		}

		public override void Update(GameTime gameTime, List<Collider> colliders)
		{
			//gravity -= gravity / (airDeceleration * 0.5f);
			if (!handleInput) return;
			base.Update(gameTime, colliders);
			acceleration.Y -= acceleration.Y / (airDeceleration * 0.5f);
		}

		public void Draw(ref SpriteBatch spriteBatch)
		{
			if (!handleInput) return;
			spriteBatch.Draw(PlatformerGame.blank, position, null, Color.White, 0, Vector2.Zero, size, SpriteEffects.None, 0);
		}
    }
}