using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace YAMGP
{
	public class Enemy : PhysicsBody
    {
        enum State { None, Left, Right, Jump, Invert };
        State state = 0;
        int maxHealth = 10;
        int _health;
        float healthBarYPos = 0f;
        float rotation = 0f;
        public int health
        {
            get
            {
                return _health;
            }
            protected set
            {
                if (value < 0) value = 0;
                _health = value;
            }
        }
        Random random = new Random();
        Stopwatch sw = new Stopwatch();

        public Enemy()
        {
            health = maxHealth;
            sw.Start();
        }

		protected override void HandleInput()
		{
            if (sw.ElapsedMilliseconds > 500)
            {
                state = (State)random.Next(0, 5);
                sw.Restart();
            }

            switch (state)
            {
            case State.Left:
                input.X--;
                break;
            case State.Right:
                input.X++;
                break;
            case State.Jump:
                input.Y++;
                break;
            case State.Invert:
                gravity = -gravity;
                break;
            }

            if (state == State.Invert || state == State.Jump)
            {
                state = (State)random.Next(0, 3);
            }

            input *= 8f;
		}

		public override void Update(GameTime gameTime, List<Collider> colliders)
		{
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds * 10f;
            healthBarYPos += dt * ((gravity > 0 ? -8f : size.Y + 8f) - healthBarYPos);
            rotation += dt * ((gravity > 0 ? 0f : MathHelper.PiOver2) - rotation);
			base.Update(gameTime, colliders);
		}

		public void Draw(ref SpriteBatch spriteBatch)
		{
			spriteBatch.Draw
            (
                PlatformerGame.blank,
                position + size / 2f,
                null,
                new Color(0.2f, 0.8f, 0.2f, 0.8f),
                rotation, new Vector2(0.5f), size,
                SpriteEffects.None, 0
            );
            float barSize = 4f, padding = 1f;
            float center = position.X + size.X / 2f - (barSize + padding) * maxHealth / 2f;
            for (int i = 0; i < health; i++)
            {
			    spriteBatch.Draw
                (
                    PlatformerGame.blank,
                    new Vector2(center + i * (barSize + padding), position.Y + healthBarYPos),
                    null, new Color((maxHealth - i) / (float)maxHealth, i / (float)maxHealth, i / (float)maxHealth, 0.8f),
                    0, Vector2.Zero,
                    new Vector2(barSize, 4),
                    SpriteEffects.None, 0
                );
            }
		}
    }
}