using System;

using Microsoft.Xna.Framework;

namespace YAMGP
{
	public class LS
	{
		public static float Orientation(Vector2 a, Vector2 b, Vector2 c)
		{
			return (b.Y - a.Y) * (c.X - b.X) - (b.X - a.X) * (c.Y - b.Y);
		}

		public static bool IntersectionTest(Line a, Line b)
		{
			float o1 = Orientation(a.a, a.b, b.a);
			float o2 = Orientation(a.a, a.b, b.b);

			float o3 = Orientation(b.a, b.b, a.a);
			float o4 = Orientation(b.a, b.b, a.b);

			return o1 * o2 < 0.0 && o3 * o4 < 0.0;
		}

		public static float A(Vector2 a, Vector2 b)
		{
			return (b.Y - a.Y) / (b.X - a.X);
		}

		public static float C(Vector2 a, Vector2 b)
		{
			return -A(a, b) * a.X + a.Y;
		}

		public static Vector2 IntersectionPoint(Line a, Line b)
		{
			if (!IntersectionTest(a, b)) return new Vector2(-1);

			float aAB = A(a.a, a.b);
			float aCD = A(b.a, b.b);

			float cAB = C(a.a, a.b);
			float cCD = C(b.a, b.b);

			float x = (cCD - cAB) / (aAB - aCD);
			float y = (cAB * aCD - cCD * aAB) / (-aAB + aCD);

			return new Vector2(x, y);
		}

		public static InterPoint RaySegment(Line ray, Line segment)
		{
			float rPX = ray.a.X;
			float rPY = ray.a.Y;

			float rDX = ray.b.X - ray.a.X;
			float rDY = ray.b.Y - ray.a.Y;

			float sPX = segment.a.X;
			float sPY = segment.a.Y;

			float sDX = segment.b.X - segment.a.X;
			float sDY = segment.b.Y - segment.a.Y;

			float rMag = (float)Math.Sqrt(rDX * rDX + rDY * rDY);
			float sMag = (float)Math.Sqrt(sDX * sDX + sDY * sDY);

			if (Math.Abs(rDX / rMag - sDX / sMag) < 0.00001f || Math.Abs(rDY / rMag - sDY / sMag) < 0.00001f)
				return null;

			float T2 = (rDX * (sPY - rPY) + rDY * (rPX - sPX)) / (sDX * rDY - sDY * rDX);
			float T1 = (sPX + sDX * T2 - rPX) / rDX;

			if (T1 <= 0 || T2 < 0 || T2 > 1) return null;

			return new InterPoint(new Vector2(rPX + rDX * T1, rPY + rDY * T1), T1);
		}
	}
}