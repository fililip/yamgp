using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace YAMGP
{
	public class Player : PhysicsBody
    {
		bool lastGrounded = false,
			 lastV = false,
			 canV = false,
			 canF = false;

		public Player()
		{
			airDeceleration = 0f;
		}

		protected override void HandleInput()
		{
			Vector2 axes = GamePad.GetState(0).ThumbSticks.Left;

			if (CheckKey(Keys.A)) input.X += -1;
            if (CheckKey(Keys.D)) input.X += 1;

			if (Math.Abs(axes.X) > float.Epsilon || Math.Abs(axes.Y) > float.Epsilon)
				axes.Normalize();
			
			input.X += axes.X;

            if (CheckKey(Keys.Space) || CheckButton(Buttons.A)) input.Y = 1;

            if ((CheckKey(Keys.F) || CheckButton(Buttons.B)) && canF)
			{
				acceleration.X *= 2.0f;
				canF = false;
			}

            input.X *= 32f;
		}

		public override void Update(GameTime gameTime, List<Collider> colliders)
		{
			bool v = CheckKey(Keys.V) || CheckButton(Buttons.X);
			if (lastV != v)
			{
				if (v && canV)
				{
					grounded = false;
					gravity *= -1f;
					acceleration.Y += gravity / 8f;
					canV = false;
				}
				lastV = v;
			}

            base.Update(gameTime, colliders);

			if (lastGrounded != grounded)
			{
				if (grounded)
				{
					canF = true;
					canV = true;
				}
				lastGrounded = grounded;
			}
		}

		public void Draw(ref SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(PlatformerGame.blank, position, null, Color.White, 0, Vector2.Zero, size, SpriteEffects.None, 0);
		}
    }
}