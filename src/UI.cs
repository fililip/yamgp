using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace YAMGP
{
	namespace UI
	{
		class Element
		{
			private Dictionary<string, object> attributes = new Dictionary<string, object>();
			public bool active = true;
			public bool ignoreInput = false;
			public int layer = 0;

			public void SetAttribute(string name, object value) => attributes[name] = value;
			public T GetAttribute<T>(string name)
			{
				return attributes.ContainsKey(name) ? (T)attributes[name] : default(T);
			}
			
			public Vector2 position = new Vector2(0, 0), size = new Vector2(32, 32);
			public Vector2 relativePosition
			{
				get
				{
					return new Vector2(GetAttribute<float>("x"), GetAttribute<float>("y"));
				}
				set
				{
					SetAttribute("x", value.X);
					SetAttribute("y", value.Y);
				}
			}
			public Color color = new Color(1f, 1f, 1f, 1f);
			public virtual void Update(GameTime gameTime) {}
			public virtual void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch) {}
		}

		class Rect : Element
		{
			public Rect() {}

			public Rect(Vector2 position, Vector2 size)
			{
				this.position = position;
				this.size = size;
			}

			public override void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch)
			{
				if (active)
					spriteBatch.Draw(PlatformerGame.blank, new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y), null, color);
			}
		}

		class Button : Element
		{
			public bool checkbox = false;
			public Text text = new Text();
			public Texture2D texture;
			public Action callback;
			public bool checkboxState = true;
			private Color _color;
			private bool previousClick = false;
			private int state = 0;
			private bool click = false;

			public Button()
			{
				texture = PlatformerGame.blank;
				size = new Vector2(128, 32);
				_color = color = new Color(0.35f, 0.35f, 0.35f, 1f);
				callback = new Action(() => {});
			}

			private bool CheckMouse()
			{
				return Mouse.GetState().Position.X > position.X &&
					   Mouse.GetState().Position.X < position.X + size.X &&
					   Mouse.GetState().Position.Y > position.Y &&
					   Mouse.GetState().Position.Y < position.Y + size.Y;
			}

			public override void Update(GameTime gameTime)
			{
				bool inside = CheckMouse();

				_color = color;
				_color.A = text.color.A = (inside && !ignoreInput) ? (state == 1 ? (byte)80 : (byte)190) : color.A;

				click = Mouse.GetState().LeftButton == ButtonState.Pressed;
				if (previousClick != click && active && !ignoreInput)
				{
					if (click)
					{
						if (state == 0 && inside) state = 1;
					} else
					{
						if (state == 1 && inside)
						{
							checkboxState = checkbox ? !checkboxState : true;
							callback();
							state = 0;
						} else state = 0;
					}
					previousClick = click;
				}

				if (checkbox)
				{
					_color.A = !checkboxState ? (byte)(MathHelper.Max(_color.A - 80, 80)) : _color.A;
					text.color.A = _color.A;
				}

				text.position = position + size / 2 - text.font.MeasureString(text.text) / 2;
				text.position = new Vector2((int)text.position.X, (int)text.position.Y);
			}

			public override void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch)
			{
				if (active)
				{
					spriteBatch.Draw(texture, new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y), null, _color);
					text.Draw(device, ref spriteBatch);
				}
			}
		}

		class HSlider : Element
		{
			public bool colorChange = true;
			public double value = 0.0;
			public float handleSize = 8f;
			public Rect background = new Rect();
			public Rect fill = new Rect();
			public Rect handle = new Rect();
			private bool previousClick = false;
			private bool click = false;
			public bool grabbed { get; protected set; } = false;
			public Action callback = () => {};
			private float grabPosition = 0f;

			public HSlider()
			{
				background.color = new Color(0.25f, 0.25f, 0.25f, 1f);
				color = new Color(0.45f, 0.45f, 0.45f, 1f);
				handle.color = Color.White;
			}

			private bool CheckMouseSlider()
			{
				return
					Mouse.GetState().Position.X > handle.position.X &&
					Mouse.GetState().Position.X < handle.position.X + handle.size.X &&
					Mouse.GetState().Position.Y > handle.position.Y &&
					Mouse.GetState().Position.Y < handle.position.Y + handle.size.Y;
			}

			private bool CheckMouse()
			{
				bool bg =
					Mouse.GetState().Position.X > position.X &&
					Mouse.GetState().Position.X < position.X + size.X &&
					Mouse.GetState().Position.Y > position.Y &&
					Mouse.GetState().Position.Y < position.Y + size.Y;

				return bg || CheckMouseSlider();
			}

			private double Clamp(double value, double min, double max)
			{
				double result = value;
				if (value.CompareTo(max) > 0)
					result = max;
				else if (value.CompareTo(min) < 0)
					result = min;
				return result;
			}

			public override void Update(GameTime gameTime)
			{
				background.position = position;
				background.size = size;

				value = Clamp(value, 0.0, 1.0);

				if (!grabbed) handle.position = background.position + new Vector2((float)(value * (size.X - handleSize)), 0);
				handle.size = new Vector2(handleSize, size.Y);

				click = Mouse.GetState().LeftButton == ButtonState.Pressed;

				if (previousClick != click && active && !ignoreInput)
				{
					if (click)
					{
						if (CheckMouseSlider())
						{
							grabbed = true;
							grabPosition = Mouse.GetState().Position.X - handle.position.X;
						} else if (CheckMouse())
						{
							grabbed = true;
							grabPosition = handleSize / 2f;
						}
					} else
					{
						if (grabbed) callback();
						grabbed = false;
					}
					previousClick = click;
				}

				if (grabbed)
				{
					handle.position.X = MathHelper.Clamp(Mouse.GetState().Position.X - grabPosition, background.position.X, background.position.X + (size.X - handleSize));
					value = (handle.position.X - background.position.X) / (size.X - handleSize);
				}

				fill.position = background.position;
				fill.size = new Vector2((float)(value * size.X), size.Y);
				if (colorChange)
				{
					fill.color = color * (float)Math.Log10(value * 9 + 1);
					fill.color.A = color.A;
				} else fill.color = color;
			}

			public override void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch)
			{
				if (active)
				{
					background.Draw(device, ref spriteBatch);
					fill.Draw(device, ref spriteBatch);
					handle.Draw(device, ref spriteBatch);
				}
			}
		}
		
		class VSlider : Element
		{
			public bool colorChange = true;
			public double value = 0.0;
			public float handleSize = 8f;
			public Rect background = new Rect();
			public Rect fill = new Rect();
			public Rect handle = new Rect();
			private bool previousClick = false;
			private bool click = false;
			public bool grabbed { get; protected set; } = false;
			public Action callback = () => {};
			private float grabPosition = 0f;

			public VSlider()
			{
				background.color = new Color(0.25f, 0.25f, 0.25f, 1f);
				color = new Color(0.45f, 0.45f, 0.45f, 1f);
				handle.color = Color.White;
			}

			private bool CheckMouseSlider()
			{
				return
					Mouse.GetState().Position.X > handle.position.X &&
					Mouse.GetState().Position.X < handle.position.X + handle.size.X &&
					Mouse.GetState().Position.Y > handle.position.Y &&
					Mouse.GetState().Position.Y < handle.position.Y + handle.size.Y;
			}

			private bool CheckMouse()
			{
				bool bg =
					Mouse.GetState().Position.X > position.X &&
					Mouse.GetState().Position.X < position.X + size.X &&
					Mouse.GetState().Position.Y > position.Y &&
					Mouse.GetState().Position.Y < position.Y + size.Y;

				return bg || CheckMouseSlider();
			}

			private double Clamp(double value, double min, double max)
			{
				double result = value;
				if (value.CompareTo(max) > 0)
					result = max;
				else if (value.CompareTo(min) < 0)
					result = min;
				return result;
			}

			public override void Update(GameTime gameTime)
			{
				background.position = position;
				background.size = size;

				value = Clamp(value, 0.0, 1.0);

				if (!grabbed) handle.position = background.position + new Vector2(0, (float)(value * (size.Y - handleSize)));
				handle.size = new Vector2(size.X, handleSize);

				click = Mouse.GetState().LeftButton == ButtonState.Pressed;

				if (previousClick != click && active && !ignoreInput)
				{
					if (click)
					{
						if (CheckMouseSlider())
						{
							grabbed = true;
							grabPosition = Mouse.GetState().Position.Y - handle.position.Y;
						} else if (CheckMouse())
						{
							grabbed = true;
							grabPosition = handleSize / 2f;
						}
					} else
					{
						if (grabbed) callback();
						grabbed = false;
					}
					previousClick = click;
				}

				if (grabbed)
				{
					handle.position.Y = MathHelper.Clamp(Mouse.GetState().Position.Y - grabPosition, background.position.Y, background.position.Y + (size.Y - handleSize));
					value = (handle.position.Y - background.position.Y) / (size.Y - handleSize);
				}

				fill.position = background.position;
				fill.size = new Vector2(size.X, (float)(value * size.Y));
				if (colorChange)
				{
					fill.color = color * (float)Math.Log10(value * 9.0 + 1.0);
					fill.color.A = color.A;
				} else fill.color = color;
			}

			public override void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch)
			{
				if (active)
				{
					background.Draw(device, ref spriteBatch);
					fill.Draw(device, ref spriteBatch);
					handle.Draw(device, ref spriteBatch);
				}
			}
		}

		class Text : Element
		{
			public SpriteFont font;
			public float scale = 1f;
			public string text = "";

			public Text()
			{
				font = PlatformerGame.font;
			}

			public override void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch)
			{
				spriteBatch.DrawString(font, text, position, color, 0, Vector2.Zero, scale, SpriteEffects.None, 0);
			}
		}

		class HList : Element
		{
			public int padding = 4;
			public int maxHeight = 0;
			public Panel panel = new Panel();
			private HSlider slider = new HSlider();

			public HList()
			{
				slider.colorChange = false;
				slider.color.A = 0;
			}

			public float TotalSize()
			{
				float size = 0f;
				foreach (Element e in panel.elements)
					size += e.size.X + (e.size.X > 0 ? padding : 0);
				return size;
			}

			public override void Update(GameTime gameTime)
			{
				panel.position = position;
				panel.size = size;
				slider.position = new Vector2(position.X, position.Y + size.Y + 8);
				slider.size = new Vector2(size.X, 8);

				float sizeDelta = TotalSize() - padding - size.X;
				bool sliderRequired = sizeDelta > 0;
				float sliderOffset = sliderRequired ? (float)(sizeDelta * slider.value) : 0;

				for (int i = 0; i < panel.elements.Count; i++)
				{
					float offset = 0f;
					for (int j = 0; j < i; j++)
						offset += panel.elements[j].size.X + (panel.elements[j].size.X > 0 ? padding : 0);
					panel.elements[i].size.Y = maxHeight != 0 ? Math.Min(size.Y, maxHeight) : size.Y;
					panel.elements[i].relativePosition = new Vector2
					(
						(float)(offset - sliderOffset),
						(maxHeight != 0 ? panel.size.Y / 2f - maxHeight / 2f : 0f)
					);
				}
				
				slider.value = !sliderRequired ? 0 : slider.value;
				slider.ignoreInput = !sliderRequired;
				slider.background.color.A = !sliderRequired ? (byte)90 : (byte)220;
				slider.handle.color.A = !sliderRequired ? (byte)0 : (byte)255;

				panel.Update(gameTime);
				slider.Update(gameTime);
			}

			public override void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch)
			{
				panel.Draw(device, ref spriteBatch);
				slider.Draw(device, ref spriteBatch);
			}
		}

		class VList : Element
		{
			public int padding = 4;
			public int maxWidth = 0;
			public Panel panel = new Panel();
			private VSlider slider = new VSlider();

			public VList()
			{
				slider.colorChange = false;
				slider.color.A = 0;
			}

			public float TotalSize()
			{
				float size = 0f;
				foreach (Element e in panel.elements)
					size += e.size.Y + (e.size.Y > 0 ? padding : 0);
				return size;
			}

			public override void Update(GameTime gameTime)
			{
				panel.position = position;
				panel.size = size;
				slider.position = new Vector2(position.X + size.X + 8, position.Y);
				slider.size = new Vector2(8, size.Y);

				float sizeDelta = TotalSize() - padding - size.Y;
				bool sliderRequired = sizeDelta > 0;
				float sliderOffset = sliderRequired ? (float)(sizeDelta * slider.value) : 0;

				for (int i = 0; i < panel.elements.Count; i++)
				{
					float offset = 0f;
					for (int j = 0; j < i; j++)
						offset += panel.elements[j].size.Y + (panel.elements[j].size.Y > 0 ? padding : 0);
					panel.elements[i].size.X = maxWidth != 0 ? Math.Min(size.X, maxWidth) : size.X;
					panel.elements[i].relativePosition = new Vector2
					(
						(maxWidth != 0 ? panel.size.X / 2f - maxWidth / 2f : 0f),
						(float)(offset - sliderOffset)
					);
				}
				
				slider.value = !sliderRequired ? 0 : slider.value;
				slider.ignoreInput = !sliderRequired;
				slider.background.color.A = !sliderRequired ? (byte)90 : (byte)220;
				slider.handle.color.A = !sliderRequired ? (byte)0 : (byte)255;

				panel.Update(gameTime);
				slider.Update(gameTime);
			}

			public override void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch)
			{
				panel.Draw(device, ref spriteBatch);
				slider.Draw(device, ref spriteBatch);
			}
		}

		class Panel : Element
		{
			public List<Element> elements = new List<Element>();
			public bool overflow = false;

			private bool CheckMouse()
			{
				bool panel =
					Mouse.GetState().Position.X > position.X &&
					Mouse.GetState().Position.X < position.X + size.X &&
					Mouse.GetState().Position.Y > position.Y &&
					Mouse.GetState().Position.Y < position.Y + size.Y;

				return panel;
			}

			public override void Update(GameTime gameTime)
			{
				foreach (Element e in elements) 
				{
					e.position = position + e.relativePosition;
					e.ignoreInput = ignoreInput || (!overflow && !CheckMouse());
					e.Update(gameTime);
				}
			}

			public override void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch)
			{
				if (!overflow)
				{
					spriteBatch.End();
					device.ScissorRectangle = new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
					spriteBatch.Begin(blendState: BlendState.NonPremultiplied, rasterizerState: PlatformerGame.scissorState);
				}

				foreach (Element e in elements) e.Draw(device, ref spriteBatch);

				if (!overflow)
				{
					spriteBatch.End();
					spriteBatch.Begin(blendState: BlendState.NonPremultiplied, rasterizerState: PlatformerGame.defaultState);
				}
			}
		}

		class Canvas
		{
			public int layer = 0;

			public List<Element> elements = new List<Element>();

			public void Sort()
			{
				elements.Sort((a, b) =>
				{
					return a.layer > b.layer ? 1 : -1;
				});
			}

			public void Update(GameTime gameTime)
			{
				foreach (Element e in elements) 
				{
					e.ignoreInput = e.layer != layer;
					e.Update(gameTime);
				}
			}

			public void Draw(GraphicsDevice device, ref SpriteBatch spriteBatch)
			{
				foreach (Element e in elements) e.Draw(device, ref spriteBatch);
			}
		}
	}
}