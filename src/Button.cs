using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace YAMGP
{
	public class Button : Collider
	{
		public List<Collider> affectedColliders = new List<Collider>();
		public int deactivationTime = -1;

		public bool pressed { get; protected set; } = false;

		bool previousAction = false;
		Stopwatch timer = new Stopwatch();

		void Initialize()
		{
			collide = false;
		}

		public Button() => Initialize();

        public Button(Vector4 dimensions, int deactivationTime = -1)
        {
			Initialize();

			position.X = dimensions.X;
            position.Y = dimensions.Y;
            size.X = dimensions.Z;
            size.Y = dimensions.W;

			this.deactivationTime = deactivationTime;
        }

		void Press()
		{
			pressed = true;
			timer.Restart();
		}

		public override void Update(GameTime gameTime)
		{
			bool action = Keyboard.GetState().IsKeyDown(Keys.Enter);
			if (action != previousAction)
			{
				if (action && playerCollision) Press();
				previousAction = action;
			}

			if (timer.ElapsedMilliseconds >= deactivationTime)
			{
				if (pressed) pressed = false;
				timer.Reset();
			}
		}
	}
}