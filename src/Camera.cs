using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.Xna.Framework;

namespace YAMGP
{
	public class Camera
	{
		struct Movement
		{
			public Stopwatch timer;
			public float duration, rotation, pause;
			public Vector2 position, scale;

			public Movement(float duration, Vector2 position, Vector2 scale, float rotation = 0f, float pause = 0f)
			{
				timer = new Stopwatch();
				this.duration = duration;
				this.position = position;
				this.scale = scale;
				this.rotation = rotation;
				this.pause = pause;
			}

			public void Start()
			{
				timer.Start();
			}
		}

		public Vector2 position = Vector2.Zero,
					   scale = Vector2.One,
					   viewport = Vector2.One;
		public float rotation = 0f;
		public Matrix matrix { get; protected set; } = new Matrix();
		public bool moving { get; protected set; } = false;

		List<Movement> keyframes = new List<Movement>();

		Vector2 _position = Vector2.Zero,
				_scale = Vector2.One;
		float _rotation = 0f;

		public void Move(float duration, Vector2 position, Vector2 scale = default(Vector2), float rotation = 0f, float pause = 0f)
		{
			if (scale == default(Vector2)) scale = Vector2.One;
			keyframes.Add(new Movement(duration, position, scale, rotation, pause));
		}

		public void CancelMove()
		{
			if (keyframes.Count <= 0) return;
			Movement mv = keyframes[keyframes.Count - 1];
			position = mv.position;
			rotation = mv.rotation;
			scale = mv.scale;
			keyframes.Clear();
		}

		public void Update(GameTime gameTime)
		{
			Func<float, float> easingFunc = (t) =>
			{
				return Easing.InOutExpo(t);
			};

			moving = keyframes.Count > 0;

			if (moving)
			{
				Movement mv = keyframes[0];

				moving = mv.timer.ElapsedMilliseconds < mv.duration - mv.duration / 2f;

				if (!mv.timer.IsRunning) mv.timer.Start();
				else
				{
					if (mv.timer.ElapsedMilliseconds >= mv.duration)
					{
						if (mv.timer.ElapsedMilliseconds >= mv.duration + mv.pause)
							keyframes.RemoveAt(0);
						position = mv.position;
						rotation = mv.rotation;
						scale = mv.scale;
					} else
					{
						float t = mv.timer.ElapsedMilliseconds / mv.duration;

						_position.X += easingFunc(t) * (mv.position.X - _position.X);
						_position.Y += easingFunc(t) * (mv.position.Y - _position.Y);

						_rotation += easingFunc(t) * (mv.rotation - _rotation);

						_scale.X += easingFunc(t) * (mv.scale.X - _scale.X);
						_scale.Y += easingFunc(t) * (mv.scale.Y - _scale.Y);
					}
				}
			} else
			{
				_position = position;
				_rotation = rotation;
				_scale = scale;
			}

			matrix = Matrix.CreateTranslation(-viewport.X * _scale.X / 2f, -viewport.Y * _scale.Y / 2f, 0f) *
					 Matrix.CreateScale(_scale.X, _scale.Y, 0f) *
					 Matrix.CreateRotationZ(_rotation) *
					 Matrix.CreateTranslation(-_position.X * _scale.X + viewport.X * _scale.X / 2f, -_position.Y * _scale.Y + viewport.Y * _scale.Y / 2f, 0f);
		}
	}
}