﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace YAMGP
{
    public class Line
    {
        public Vector2 a = Vector2.Zero;
        public Vector2 b = Vector2.Zero;
        public float angle = 0f;

        public Line(Vector2 a, Vector2 b, float angle = 0f)
        {
            this.a = a;
            this.b = b;
            this.angle = angle;
        }
    }

    public class InterPoint
    {
        public Vector2 position = Vector2.Zero;
        public float T = 0f;
        public float angle = 0f;

        public InterPoint(Vector2 position, float T, float angle = 0f)
        {
            this.position = position;
            this.T = T;
            this.angle = angle;
        }
    }

    public class PlatformerGame : Game
    {
        public static Texture2D blank;
        public static SpriteFont font;

        public static RasterizerState scissorState, defaultState;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Player player = new Player();
        Drone drone = new Drone();
        Camera camera = new Camera();
        Vector2 dronePosition = Vector2.Zero;
        Vector2 chunk = Vector2.Zero;
        Vector2 lastChunk = Vector2.Zero;
        float width = 2560, height = 1440;
        Vector2 chunkSize = new Vector2(1280, 720);
        
        List<Collider> colliders = new List<Collider>();
        List<Line> lines = new List<Line>();
        List<Enemy> enemies = new List<Enemy>();

        Random random = new Random();

        Socket socket = null;
        IPEndPoint ipe = null;

        UI.Canvas canvas = new UI.Canvas();
        UI.Text fpsDisplay;
        UI.Button subscribe;

        BasicEffect oldEffect;
        Effect effect;
        VertexBuffer sightBuffer;
        VertexPosition[] sightArray = new VertexPosition[50000];
        List<Vector2> sightPos = new List<Vector2>();

        bool lastE = false;

        public PlatformerGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "content";
            IsMouseVisible = true;
            IsFixedTimeStep = false;
        }

        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            camera.viewport = new Vector2(1280, 720);
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();

            /* IPHostEntry ipAddress = Dns.GetHostEntry("localhost");
            ipe = new IPEndPoint(ipAddress.AddressList[0], 8080);

            socket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(ipe); */

            base.Initialize();
        }

        protected override void LoadContent()
        {
            scissorState = new RasterizerState();
            defaultState = new RasterizerState();
            scissorState.ScissorTestEnable = true;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            blank = new Texture2D(GraphicsDevice, 1, 1);
            blank.SetData(new[] { Color.White });
            font = Content.Load<SpriteFont>("fonts/font");
            colliders.Add(new Collider(new Vector4(-32, 0, 32, height)));
            colliders.Add(new Collider(new Vector4(width, 0, 32, height)));
            colliders.Add(new Collider(new Vector4(0, -32, width, 32)));
            colliders.Add(new Collider(new Vector4(0, height, width, 32)));
            colliders.Add(new Button(new Vector4(256 + 8, 162 + 8, 16, 16), 250));
            for (int i = 0; i < 8; i++)
            {
                Collider collider = new Collider();
                collider.size = new Vector2(320, 32);
                collider.position.X = i * 180f;
                collider.position.Y = i * 130f + 64f;
                colliders.Add(collider);
            }
            for (int i = 0; i < 100; i++)
            {
                Enemy enemy = new Enemy();
                enemy.position.X = random.Next(0, 1280);
                //enemies.Add(enemy);
            }

            fpsDisplay = new UI.Text();
            subscribe = new UI.Button();

            canvas.elements.Add(fpsDisplay);

            subscribe.text.text = "subscurlurjk kanal jutub";
            subscribe.position.Y = 720f - 32f;
            subscribe.color.A = 140;
            subscribe.size.X = 280f;
            subscribe.callback = () =>
            {
                switch (Environment.OSVersion.Platform)
                {
                case PlatformID.Win32NT:
                    Process.Start("https://fililip.itch.io");
                    break;
                case PlatformID.Unix:
                    Process p = new Process();
                    p.StartInfo = new ProcessStartInfo("xdg-open");
                    p.StartInfo.Arguments = "https://fililip.itch.io";
                    p.Start();
                    break;
                }
            };
            canvas.elements.Add(subscribe);

            effect = Content.Load<Effect>("shaders/light");
            oldEffect = new BasicEffect(GraphicsDevice);
            sightBuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPosition), sightArray.Length, BufferUsage.WriteOnly);

            foreach (Collider c in colliders)
            {
                Line left = new Line(c.position, c.position + new Vector2(0f, c.size.Y));
                Line right = new Line(c.position + new Vector2(c.size.X, 0f), c.position + c.size);

                Line top = new Line(c.position, c.position + new Vector2(c.size.X, 0f));
                Line bottom = new Line(c.position + new Vector2(0f, c.size.Y), c.position + c.size);
                
                lines.Add(left);
                lines.Add(top);
                lines.Add(right);
                lines.Add(bottom);
            }

            lines.Add(new Line(new Vector2(0), new Vector2(width, 0)));
            lines.Add(new Line(new Vector2(width, 0), new Vector2(width, height)));
            lines.Add(new Line(new Vector2(width, height), new Vector2(0, height)));
            lines.Add(new Line(new Vector2(0, height), new Vector2(0)));
        }

        bool CheckKey(Keys key)
        {
            return Keyboard.GetState().IsKeyDown(key);
        }

        protected override void Update(GameTime gameTime)
        {
            if (CheckKey(Keys.Escape))
                Exit();
            
            bool e = CheckKey(Keys.E);
            if (e != lastE)
            {
                if (e && player.AABB(new Collider(new Vector4(drone.position.X, drone.position.Y, drone.size.X, drone.size.Y))))
                {
                    drone.handleInput = !drone.handleInput;
                    player.handleInput = !drone.handleInput;
                    drone.acceleration = Vector2.Zero;
                    if (player.handleInput)
                        camera.Move(1100, chunk * chunkSize);
                    else
                        camera.Move(1100, dronePosition);
                }
                lastE = e;
            }
            
            if (!camera.moving)
            {
                for (int i = 0; i < enemies.Count; i++)
                {
                    Enemy enemy = enemies[i];
                    enemy.Update(gameTime, colliders);
                    if (enemy.health == 0)
                    {
                        enemies.RemoveAt(i);
                        i--;
                    }
                }
                foreach (Collider collider in colliders)
                {
                    collider.Update(gameTime);
                    switch (collider)
                    {
                        case Button button:
                            if (button.pressed) Console.WriteLine(DateTime.Now.Millisecond);
                            break;
                    }
                }
                player.Update(gameTime, colliders);
                drone.Update(gameTime, colliders);
            }

            oldEffect.Projection = camera.matrix * Matrix.CreateOrthographicOffCenter(0f, 1280f, 720f, 0f, 0f, 1000f);
            effect.Parameters["projection"].SetValue(camera.matrix * Matrix.CreateOrthographicOffCenter(0f, 1280f, 720f, 0f, 0f, 1000f));
            effect.Parameters["color"].SetValue(new Vector4(0.85f, 0.25f, 0.25f, 0.8f));

            dronePosition = drone.position - new Vector2(1280, 720) / 2f;
            dronePosition = Vector2.Clamp(dronePosition, Vector2.Zero, new Vector2(width, height) - new Vector2(1280, 720));

            if (drone.handleInput)
                camera.position = dronePosition;
            else
            {
                drone.position = player.position;
                chunk = new Vector2((float)Math.Floor(player.position.X / chunkSize.X), (float)Math.Floor(player.position.Y / chunkSize.Y));
                if (lastChunk != chunk)
                {
                    camera.CancelMove();
                    camera.Move(1100, chunk * chunkSize);
                    lastChunk = chunk;
                }
            }

            fpsDisplay.text = "duzo fps";

            canvas.Update(gameTime);
            camera.Update(gameTime);

            if (CheckKey(Keys.C)) camera.CancelMove();

            List<Vector2> points = new List<Vector2>();
            List<float> angles = new List<float>();

            Vector2 lightSourcePos = player.position + player.size / 2f;

            foreach (Line line in lines)
            {
                Action<Vector2> addPoint = (v) =>
                {
                    float angle = (float)Math.Atan2(v.Y - lightSourcePos.Y, v.X - lightSourcePos.X);
                    angles.Add(angle - 0.00001f);
                    angles.Add(angle);
                    angles.Add(angle + 0.00001f);
                    points.Add(v);
                };

                if (!points.Contains(line.a)) addPoint(line.a);
                if (!points.Contains(line.b)) addPoint(line.b);
            }

            List<InterPoint> sight = new List<InterPoint>();

            foreach (float angle in angles)
            {
                Line ray = new Line
                (
                    lightSourcePos,
                    lightSourcePos + new Vector2
                    (
                        (float)(Math.Cos(angle) * 10000),
                        (float)(Math.Sin(angle) * 10000)
                    )
                );

                InterPoint point = null;
                float T = 0f;

                foreach (Line line in lines)
                {
                    InterPoint ip = LS.RaySegment(ray, line);

                    if (ip == null) continue;

                    if (ip.T < T || point == null)
                    {
                        point = ip;
                        point.angle = angle;
                        T = ip.T;
                    }
                }

                if (point != null)
                {
                    sight.Add(point);
                }
            }

            sight.Sort((a, b) =>
            {
                if (Math.Abs(a.angle - b.angle) < 0.0000001f) return 1;
                return a.angle > b.angle ? 1 : -1;
            });

            sightPos.Clear();

            for (int i = 0; i < sight.Count - 1; i++)
            {
                sightPos.Add(lightSourcePos);
                sightPos.Add(sight[i].position);
                sightPos.Add(sight[i + 1].position);
            }

            sightPos.Add(lightSourcePos);
            sightPos.Add(sight[sight.Count - 1].position);
            sightPos.Add(sight[0].position);

            for (int i = 0; i < sightPos.Count; i++)
                sightArray[i].Position = new Vector3(sightPos[i].X, sightPos[i].Y, 0);
            
            sightBuffer.SetData(sightArray);

            /* effect.Parameters["resolution"].SetValue
            (
                new Vector2
                (
                    graphics.PreferredBackBufferWidth,
                    graphics.PreferredBackBufferHeight
                )
            );
            effect.Parameters["position"].SetValue(lightSourcePos); */

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            GraphicsDevice.SetVertexBuffer(sightBuffer);

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, sightPos.Count);
            }

            spriteBatch.Begin(blendState: BlendState.NonPremultiplied, transformMatrix: camera.matrix);
            foreach (Collider collider in colliders)
                spriteBatch.Draw(blank, new Vector2(collider.position.X, collider.position.Y), null, new Color(1f, 1f, 1f, 1f), 0, Vector2.Zero, new Vector2(collider.size.X, collider.size.Y), SpriteEffects.None, 0); 
            foreach (Enemy enemy in enemies) enemy.Draw(ref spriteBatch);
            player.Draw(ref spriteBatch);
            drone.Draw(ref spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin(blendState: BlendState.NonPremultiplied);
            canvas.Draw(GraphicsDevice, ref spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
