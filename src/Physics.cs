using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace YAMGP
{
    public class Collider
    {
        public Dictionary<string, string> attributes = new Dictionary<string, string>();
        public Vector2 position;
        public Vector2 size;
        public bool collide = true;
        public bool playerCollision = false;

        public Collider() {}

        public Collider(Vector4 dimensions)
        {
            position.X = dimensions.X;
            position.Y = dimensions.Y;
            size.X = dimensions.Z;
            size.Y = dimensions.W;
        }

        public virtual void Update(GameTime gameTime) {}
    }

	public class PhysicsBody
    {
        public static float globalGravity = 9.81f;
		public Vector2 position = Vector2.Zero,
					   size = new Vector2(32),
        			   acceleration = Vector2.Zero,
					   input = Vector2.Zero,
					   maxSpeed = new Vector2(2.5f, 10f);
		public float gravity = 9.81f,
					 jumpStrength = 5f,
					 groundAcceleration = 2.5f,
					 groundDeceleration = 6.5f,
					 airAcceleration = 0.75f,
					 airDeceleration = 38f;
        public bool grounded { get; protected set; } = false;
        public bool handleInput = true;

        bool lastGrounded = false,
             decelerate = false;
        public Stopwatch groundTimer = new Stopwatch();

		protected bool CheckKey(Keys key)
        {
            return Keyboard.GetState().IsKeyDown(key);
        }

		protected bool CheckButton(Buttons button)
        {
            return GamePad.GetState(0).IsButtonDown(button);
        }

		public Vector2 CollisionCheck(Collider collider, Vector2 direction)
        {
            Vector2 futurePos = position + direction;

            bool collision = futurePos.X + size.X > collider.position.X &&
                             futurePos.X < collider.position.X + collider.size.X &&
                             futurePos.Y + size.Y > collider.position.Y &&
                             futurePos.Y < collider.position.Y + collider.size.Y;

            Vector2 returnV = Vector2.Zero;
            if (!collision) return returnV;

            if (futurePos.X + size.X > collider.position.X &&
                position.X < collider.position.X + collider.size.X &&
                position.Y + size.Y > collider.position.Y &&
                position.Y < collider.position.Y + collider.size.Y) returnV.X = -1;

            if (position.X + size.X > collider.position.X &&
                futurePos.X < collider.position.X + collider.size.X &&
                position.Y + size.Y > collider.position.Y &&
                position.Y < collider.position.Y + collider.size.Y) returnV.X = 1;

            if (position.X + size.X > collider.position.X &&
                position.X < collider.position.X + collider.size.X &&
                futurePos.Y + size.Y > collider.position.Y &&
                position.Y < collider.position.Y + collider.size.Y) returnV.Y = -1;

            if (position.X + size.X > collider.position.X &&
                position.X < collider.position.X + collider.size.X &&
                position.Y + size.Y > collider.position.Y &&
                futurePos.Y < collider.position.Y + collider.size.Y) returnV.Y = 1;
            
            if (position.X + size.X > collider.position.X &&
                position.X < collider.position.X + collider.size.X &&
                position.Y + size.Y > collider.position.Y &&
                position.Y < collider.position.Y + collider.size.Y) returnV = new Vector2(2);

            return returnV;
        }

		protected virtual void HandleInput() {}

        public bool AABB(Collider collider)
        {
            return
            position.X + size.X > collider.position.X &&
            position.X < collider.position.X + collider.size.X &&
            position.Y + size.Y > collider.position.Y &&
            position.Y < collider.position.Y + collider.size.Y;
        }

		public virtual void Update(GameTime gameTime, List<Collider> colliders)
		{
			input = Vector2.Zero;

            if (handleInput) HandleInput();

            float accelXFactor = input.X *
                                 (float)gameTime.ElapsedGameTime.TotalSeconds *
                                 (grounded ? groundAcceleration : airAcceleration);
            
            if (Math.Abs(acceleration.X + accelXFactor) < maxSpeed.X * 1.25f)
                acceleration.X += accelXFactor;

            if (acceleration.X != 0f) grounded = false;

            if (!grounded)
                acceleration.Y += (float)gameTime.ElapsedGameTime.TotalSeconds * gravity;

            //acceleration.X = MathHelper.Clamp(acceleration.X, -maxSpeed.X, maxSpeed.X);
            //acceleration.Y = MathHelper.Clamp(acceleration.Y, -maxSpeed.Y, maxSpeed.Y);

            Action<Collider, bool> setCollision = (c, b) => c.playerCollision = b;

            foreach (Collider collider in colliders)
            {
                Vector2 direction = CollisionCheck(collider, acceleration);
                setCollision(collider, false);

                if (direction == new Vector2(2)) setCollision(collider, GetType() == typeof(Player));

                if (!collider.collide) continue;

                if (direction.X != 0) acceleration.X = 0f;
                if (direction.Y != 0) acceleration.Y = 0f;

                switch (direction.X)
                {
                case -1:
                    position.X = collider.position.X - size.X;
                    break;
                case 1:
                    position.X = collider.position.X + collider.size.X;
                    break;
                }

                switch (direction.Y)
                {
                case -1:
                    if (gravity > 0f) grounded = true;
                    position.Y = collider.position.Y - size.Y;
                    break;
                case 1:
                    if (gravity < 0f) grounded = true;
                    position.Y = collider.position.Y + collider.size.Y;
                    break;
                }
            }

            position += acceleration;

            decelerate = groundTimer.ElapsedMilliseconds > 6;

            if (decelerate && grounded)
                acceleration.X -= groundDeceleration != 0f ? acceleration.X / groundDeceleration : 0f;
            else
                acceleration.X -= airDeceleration != 0f ? acceleration.X / airDeceleration : 0f;
            
            if (Math.Abs(acceleration.X) < 0.001f) acceleration.X = 0f;

            if (grounded && input.Y == 1)
            {
                grounded = false;
                acceleration.Y -= jumpStrength * Math.Sign(gravity);
            }

            if (grounded != lastGrounded)
            {
                if (grounded) groundTimer.Restart();
                else groundTimer.Reset();
                lastGrounded = grounded;
            }
		}

        public Vector2 Raycast(Vector2 origin, List<Collider> colliders)
		{
			int steps = 10;
            Vector2 lineEnd = origin;
            bool found = false;

            for (int i = 0; i < steps; i++)
            {
                foreach (Collider collider in colliders)
                {
                    if (CollisionCheck(collider, new Vector2(0, lineEnd.Y - position.Y)).Y == -1)
                    {
                        lineEnd.Y = collider.position.Y - collider.size.Y;
                        found = true;
                        break;
                    }
                }
                if (found) break;
                if (i < steps - 1) lineEnd.Y += 8f;
            }

			return lineEnd;
		}
	}
}