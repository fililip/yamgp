using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace YAMGP
{
	public class Particle : PhysicsBody
	{
		public bool alive = true;
        public bool lastGrounded = false;
		float rotation = 0;

		Random random = new Random();
		Stopwatch sw = new Stopwatch();

		public Particle()
		{
			size = new Vector2(4);
			rotation = (float)random.NextDouble() * 3.14152f;
			gravity = 2.4f;
		}

		public override void Update(GameTime gameTime, List<Collider> colliders)
		{
			base.Update(gameTime, colliders);

			if (!grounded) rotation += (float)gameTime.ElapsedGameTime.TotalSeconds;

			if (grounded != lastGrounded)
			{
				if (grounded) sw.Start();
				lastGrounded = grounded;
			}

			if (sw.ElapsedMilliseconds > 200) alive = false;
		}

		public void Draw(ref SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(PlatformerGame.blank, position - new Vector2(4), null, new Color(0.8f, 0.8f, 0.2f, 0.8f), rotation, new Vector2(-2), size, SpriteEffects.None, 0);
		}
	}

	public class ParticleSystem
	{
		public int spawnRate = 10;
		public Vector4 origin = Vector4.Zero;
		List<Particle> particles = new List<Particle>();
		Stopwatch sw = new Stopwatch();
		Random random = new Random();

		public ParticleSystem()
		{
			sw.Start();
		}

		public void Update(GameTime gameTime, List<Collider> colliders)
		{
			if (sw.ElapsedMilliseconds > 150)
			{
				for (int i = 0; i < spawnRate; i++)
				{
					Particle particle = new Particle();
					particle.position.X = random.Next((int)origin.X, (int)(origin.X + origin.Z));
					particle.position.Y = random.Next((int)origin.Y, (int)(origin.Y + origin.W));
					particles.Add(particle);
				}
				sw.Restart();
			}

			for (int i = 0; i < particles.Count; i++)
			{
				Particle p = particles[i];
				p.Update(gameTime, colliders);
				if (!p.alive)
				{
					particles.RemoveAt(i);
					i--;
				}
			}
		}

		public void Draw(ref SpriteBatch spriteBatch)
		{
			foreach (Particle particle in particles)
				particle.Draw(ref spriteBatch);
		}
	}
}