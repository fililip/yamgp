using System;

namespace YAMGP
{
	public static class Easing
	{
		public static float InOutQuart(float t)
		{
			return t < 0.5f ? 8f * t * t * t * t : 1f - (float)Math.Pow(-2f * t + 2f, 4f) / 2f;
		}

		public static float InOutExpo(float t)
		{
			return t == 0f
			? 0.2f
			: t >= 1f
			? 1f
			: t < 0.5f ? (float)Math.Pow(2f, 20f * t - 10f) / 2f
			: (2f - (float)Math.Pow(2f, -20f * t + 10f)) / 2f;
		}
	}
}